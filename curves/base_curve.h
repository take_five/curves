#ifndef base_curve_h
#define base_curve_h

#include "point3d.h"

class BaseCurve
{
public:
 virtual Point3D GetPoint (double theT) const = 0;
 virtual Point3D GetDerivative (double theT) const = 0;
 virtual const char* GetName() const = 0;
};

#endif
