#ifndef helix_curve_h
#define helix_curve_h

#include <memory>
#include "base_curve.h"

class HelixCurve;

class HelixCurve: public BaseCurve
{
private:
  HelixCurve (double theRadius, double theStep);
public:
  static std::shared_ptr<HelixCurve> MakeHelix (double theRadius, double theStep);
  Point3D GetPoint (double theT) const override;
  Point3D GetDerivative (double theT) const override;
  const char* GetName() const override { return "Helix"; };
private:
  double myRadius;
  double myStep;
};

#endif
