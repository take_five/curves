#ifndef point3d_h
#define point3d_h

struct Point3D
{
  double x;
  double y;
  double z;
};

#endif
