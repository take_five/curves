#ifndef ellipse_curve_h
#define ellipse_curve_h

#include <memory>
#include "base_curve.h"

class EllipseCurve;

class EllipseCurve: public BaseCurve
{
private:
  EllipseCurve (double theRadiusX, double theRadiusY);
public:
  static std::shared_ptr<EllipseCurve> MakeEllipse (double theRadiusX, double theRadiusY);
  Point3D GetPoint (double theT) const override;
  Point3D GetDerivative (double theT) const override;
  const char* GetName() const override { return "Ellipse"; };
private:
  double myRadiusX;
  double myRadiusY;
};

#endif
