#include <cmath>
#include "circle_curve.h"

CircleCurve::CircleCurve (double theRadius) :
  myRadius(theRadius)
{
}

std::shared_ptr<CircleCurve> CircleCurve::MakeCircle (double theRadius)
{
  if (theRadius > 0.0)
  {
    std::shared_ptr<CircleCurve> aPtr { new CircleCurve(theRadius) };
    return aPtr;
  }
  else
  {
    return {};
  }
}

Point3D CircleCurve::GetPoint (double theT) const
{
  Point3D aPnt;
  
  aPnt.x = myRadius * std::cos (theT);
  aPnt.y = myRadius * std::sin (theT);
  aPnt.z = 0.0;
  
  return aPnt;
}

Point3D CircleCurve::GetDerivative(double theT) const
{
  Point3D aDer;

  aDer.x = -myRadius * std::sin (theT);
  aDer.y =  myRadius * std::cos (theT);
  aDer.z =  0.0;
  
  return aDer;
}
