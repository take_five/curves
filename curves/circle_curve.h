#ifndef circle_curve_h
#define circle_curve_h

#include <memory>
#include "base_curve.h"

class CircleCurve;

class CircleCurve: public BaseCurve
{
private:
  CircleCurve (double theRadius);
public:
  static std::shared_ptr<CircleCurve> MakeCircle (double theRadius);
  Point3D GetPoint (double theT) const override;
  Point3D GetDerivative (double theT) const override;
  const char* GetName() const override { return "Circle"; };
  double GetRadius() const { return myRadius; };
private:
  double myRadius;
};

#endif
