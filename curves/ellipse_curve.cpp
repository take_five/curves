#include <cmath>
#include "ellipse_curve.h"

EllipseCurve::EllipseCurve (double theRadiusX, double theRadiusY) :
  myRadiusX(theRadiusX), myRadiusY(theRadiusY)
{
}

std::shared_ptr<EllipseCurve> EllipseCurve::MakeEllipse (double theRadiusX, double theRadiusY)
{
  if (theRadiusX > 0.0 && theRadiusY > 0.0)
  {
    std::shared_ptr<EllipseCurve> aPtr { new EllipseCurve(theRadiusX, theRadiusY) };
    return aPtr;
  }
  else
  {
    return {};
  }
}

Point3D EllipseCurve::GetPoint (double theT) const
{
  Point3D aPnt;
  
  aPnt.x = myRadiusX * std::cos (theT);
  aPnt.y = myRadiusY * std::sin (theT);
  aPnt.z = 0.0;
  
  return aPnt;
}

Point3D EllipseCurve::GetDerivative(double theT) const
{
  Point3D aDer;

  aDer.x = -myRadiusX * std::sin (theT);
  aDer.y =  myRadiusY * std::cos (theT);
  aDer.z =  0.0;
  
  return aDer;
}
