#include <cmath>
#include "helix_curve.h"
#include "const.h"

HelixCurve::HelixCurve (double theRadius, double theStep) :
  myRadius(theRadius), myStep(theStep)
{
}

std::shared_ptr<HelixCurve> HelixCurve::MakeHelix (double theRadius, double theStep)
{
  if (theRadius > 0.0)
  {
    std::shared_ptr<HelixCurve> aPtr { new HelixCurve(theRadius, theStep) };
    return aPtr;
  }
  else
  {
    return {};
  }
}

Point3D HelixCurve::GetPoint (double theT) const
{
  Point3D aPnt;
  
  aPnt.x = myRadius * std::cos (theT);
  aPnt.y = myRadius * std::sin (theT);
  aPnt.z = myStep * theT / (2.0 * gPiConst);
  
  return aPnt;
}

Point3D HelixCurve::GetDerivative(double theT) const
{
  Point3D aDer;

  aDer.x = -myRadius * std::sin (theT);
  aDer.y =  myRadius * std::cos (theT);
  aDer.z =  myStep / (2.0 * gPiConst);
  
  return aDer;
}
