# Curves

Test task

## Get Build Run

Without Intel TBB

```
mkdir ~/curves_prj
cd ~/curves_prj
git clone https://gitlab.com/take_five/curves.git
mkdir build
cd build
cmake ../curves/
make
./curves_app
```

With Intel TBB

```
mkdir ~/curves_prj
cd ~/curves_prj
git clone https://github.com/oneapi-src/oneTBB.git
mkdir tbb_build
cd tbb_build
cmake -DCMAKE_INSTALL_PREFIX=~/curves_prj/tbb -DTBB_TEST=OFF ../oneTBB/
make
make install
cd ..
git clone https://gitlab.com/take_five/curves.git
mkdir build
cd build
cmake -DTBB_DIR="~/curves_prj/tbb/lib/cmake/TBB" ../curves/
make
./curves_app
```

