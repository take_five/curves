#include <iostream>
#include <random>
#include <list>
#include <vector>
#include <algorithm>
#include "circle_curve.h"
#include "ellipse_curve.h"
#include "helix_curve.h"
#include "const.h"
#ifdef USE_TBB
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"
#include "tbb/spin_mutex.h"
#endif

int main()
{
  std::random_device aRandDev;
  std::mt19937 aRandGen(aRandDev());
  std::uniform_int_distribution<> aCurveDistr(1, 3);
  std::normal_distribution<> aParamDistr(0.0, 5.0);
  
  // populate the list1  
  std::cout << "=============================================\n";
  std::cout << "Parameters of the curves\n";
  std::list<std::shared_ptr<BaseCurve>> anAllCurves;
  while (anAllCurves.size() != 100)
  {
    switch (aCurveDistr(aRandGen))
    {
      case 1:
      {
        double aRadius = aParamDistr(aRandGen);
        std::shared_ptr<CircleCurve> aCircle = CircleCurve::MakeCircle (aRadius);
        if (aCircle)
        {
          anAllCurves.push_back(aCircle);
          std::cout << "Circle: R = " << aRadius << "\n";
        }
        break;
      }
      case 2:
      {
        double aRadiusX = aParamDistr(aRandGen);
        double aRadiusY = aParamDistr(aRandGen);
        std::shared_ptr<EllipseCurve> anEllipse = EllipseCurve::MakeEllipse (aRadiusX, aRadiusY);
        if (anEllipse)
        {
          anAllCurves.push_back(anEllipse);
          std::cout << "Ellipse: Rx = " << aRadiusX << " Ry = " << aRadiusY << "\n";
        }
        break;
      }
      case 3:
      {
        double aRadius = aParamDistr(aRandGen);
        double aStep = aParamDistr(aRandGen);
        std::shared_ptr<HelixCurve> aHelix = HelixCurve::MakeHelix (aRadius, aStep);
        if (aHelix)
        {
          anAllCurves.push_back(aHelix);
          std::cout << "Helix: R = " << aRadius << " S = " << aStep << "\n";
        }
        break;
      }
    }
  }
  
  // print points and derivatives
  std::cout << "=============================================\n";
  std::cout << "All curves\n";
  for (std::list<std::shared_ptr<BaseCurve>>::iterator anIt = anAllCurves.begin(); anIt != anAllCurves.end(); anIt++)
  {
    Point3D aPnt = (*anIt)->GetPoint(0.25 * gPiConst);
    Point3D aDer = (*anIt)->GetDerivative(0.25 * gPiConst);

    std::cout << (*anIt)->GetName() << ": \n";
    std::cout << "\tPNT: " << aPnt.x << " " << aPnt.y << " " << aPnt.z << "\n";
    std::cout << "\tDRV: " << aDer.x << " " << aDer.y << " " << aDer.z << "\n";
  }
  
  // populate second container
  std::vector<std::shared_ptr<CircleCurve>> aCircleCurves;
  for (std::list<std::shared_ptr<BaseCurve>>::iterator anIt = anAllCurves.begin(); anIt != anAllCurves.end(); anIt++)
  {
    std::shared_ptr<CircleCurve> aCircle = std::dynamic_pointer_cast<CircleCurve>(*anIt);
    if (aCircle)
    {
      aCircleCurves.push_back(aCircle);
    }
  }
  
  // print radii
  std::cout << "=============================================\n";
  std::cout << "Before sort\n";
  for (std::vector<std::shared_ptr<CircleCurve>>::size_type i = 0; i < aCircleCurves.size(); i++)
  {
    double aRadius = aCircleCurves[i]->GetRadius();

    std::cout << aRadius << "\n";
  }
  
  std::sort(aCircleCurves.begin(),
            aCircleCurves.end(),
            [](const std::shared_ptr<CircleCurve>& theC1, const std::shared_ptr<CircleCurve>& theC2)
            { return theC1->GetRadius() < theC2->GetRadius(); } );

  // print radii
  std::cout << "=============================================\n";
  std::cout << "After sort\n";
  for (std::vector<std::shared_ptr<CircleCurve>>::size_type i = 0; i < aCircleCurves.size(); i++)
  {
    double aRadius = aCircleCurves[i]->GetRadius();

    std::cout << aRadius << "\n";
  }
  
  // total sum
  std::cout << "=============================================\n";
  std::cout << "The total sum of circles' radii\n";
  double aRadiiSum = 0.0;
  for (auto aCircle : aCircleCurves)
  {
    aRadiiSum += aCircle->GetRadius();
  }
  std::cout << aRadiiSum << "\n";
#ifdef USE_TBB
  std::cout << "The total sum calculated with Intel TBB\n";
  aRadiiSum = 0.0;
  tbb::spin_mutex aSumMutex;
  tbb::parallel_for(tbb::blocked_range<std::vector<std::shared_ptr<CircleCurve>>::size_type>(0, aCircleCurves.size(), 10),
                    [&](const tbb::blocked_range<std::vector<std::shared_ptr<CircleCurve>>::size_type>& aRange)
                    {
                      double aLocalSum = 0.0;
                      for (auto i = aRange.begin(); i != aRange.end(); i++)
                      {
                        aLocalSum += aCircleCurves[i]->GetRadius();
                      }
                      tbb::spin_mutex::scoped_lock aLock(aSumMutex);
                      aRadiiSum += aLocalSum;
                    });
  std::cout << aRadiiSum << "\n";
#endif
  
  return 0;
}
